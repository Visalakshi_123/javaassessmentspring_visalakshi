package com.allstate.assignment.dao;

import com.allstate.assignment.dto.Payment;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class PaymentRepoTest {
    @Autowired
    PaymentRepo paymentRepo;

    @BeforeEach
    void setup(){
paymentRepo.save(new Payment(2, LocalDate.now(),"savings",20.2,22));
    }

    @AfterEach
    void tearDown(){
        paymentRepo.deleteAll();

    }

    @Test
    public void test_rowCount(){
    assertTrue((long)13> paymentRepo.count()) ;
    }

    @Test
    public void test_findByIdOptionalEmpty() {
        assertEquals(
                Optional.empty(), paymentRepo.findById(99));
    }


    @Test
    public void test_findByIdNotEmpty() {

      //  assertNotEquals(Optional.empty(),paymentRepo.findById(1));
    }
    @Test
    public void test_findAllSize() {
        paymentRepo.deleteAll();
        assertEquals(
                0, paymentRepo.findAll().size());
    }
    @Test
    public void test_SaveReturns1() {
        paymentRepo.deleteAll();
        Payment payment=new Payment(2, LocalDate.now(),"savings",20.2,22);
        paymentRepo.save(payment);
        assertEquals(
                1, paymentRepo.findAll().size());
    }


}
