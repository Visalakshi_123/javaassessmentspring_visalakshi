package com.allstate.assignment.PaymentControllerTest;

import com.allstate.assignment.dao.PaymentRepo;
import com.allstate.assignment.dto.Payment;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PaymentControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    PaymentRepo paymentRepo;


    @BeforeEach
    void setup(){
        paymentRepo.save(new Payment(2, LocalDate.now(),"Savings",20.2,22));
    }

    @AfterEach
    void tearDown(){
        paymentRepo.deleteAll();

    }


    @Test
    public void testStatus() {

        ResponseEntity<String> responseEntity = restTemplate.exchange(
                "/api/payment/status",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<String>() {
                });

        String responseBody = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseBody.contains("running"));
    }

    @Test
    public void testFindByID() {
        Integer id = 47;
        ResponseEntity<Payment> responseEntity = restTemplate.exchange(
                "/api/payment/find/47",
                HttpMethod.GET,
                null,
                Payment.class);

        Payment responseBody = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        //assertNotEquals(null, responseBody.getType());
    }

    @Test
    public void testFindAll() {
        Integer id = 47;
        ResponseEntity<List<Payment>> responseEntity = restTemplate.exchange(
                "/api/payment/findAll",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Payment>>() {
                });

        List<Payment> responseBody = (List<Payment>) responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(20.2, responseBody.get(0).getAmount());
    }

    @Test
    public void testSave() {
        HttpEntity<Payment> request = new HttpEntity<>(new Payment(2, LocalDate.now(), "savings", 20.2, 22));

        Integer id = 47;
        ResponseEntity<List<Payment>> responseEntity = restTemplate.exchange(
                "/api/payment/save",
                HttpMethod.POST,
                request,
                new ParameterizedTypeReference<List<Payment>>() {
                });

        List<Payment> responseBody = (List<Payment>) responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

    }

}
