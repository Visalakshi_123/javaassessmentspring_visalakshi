package com.allstate.assignment.services;


import com.allstate.assignment.dao.PaymentRepo;
import com.allstate.assignment.dto.Payment;
import com.allstate.assignment.exceptions.OutOfRangeException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class PaymentServiceTest {

    @Autowired
    PaymentRepo paymentRepo;
    @Autowired
    PaymentService paymentService;

    @BeforeEach
    void setup(){
        paymentRepo.save(new Payment(2, LocalDate.now() ,"savings",20.2,22));
    }

    @AfterEach
    void tearDown(){
        paymentRepo.deleteAll();
    }

    @Test
    public void testFindByIDThrowsException() {

        Exception exception = assertThrows(OutOfRangeException.class, () -> {
            Optional<Payment> payment = paymentService.find(0);
        });
    }

    @Test
    public void testSaveAlreadyPresentIDThrowsException() {
        Payment payment = new Payment(0, LocalDate.now(), "savings", 20.2, 22);
        Exception exception = assertThrows(OutOfRangeException.class, () -> {
            paymentService.save(payment);
        });
    }

    @Test
    public void testFindReturnsNullIfNoRecords() throws OutOfRangeException {
        paymentRepo.deleteAll();
        assertEquals(null, paymentService.find(3));

    }

    @Test
    public void testFindAllReturnsEmptyIfNoRecords() throws OutOfRangeException {
        paymentRepo.deleteAll();
        assertTrue(paymentService.all().isEmpty());

    }


}