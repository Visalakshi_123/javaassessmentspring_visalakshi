package com.allstate.assignment.services;

import com.allstate.assignment.dao.PaymentRepo;
import com.allstate.assignment.dto.Payment;

import com.allstate.assignment.exceptions.OutOfRangeException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class PaymentServiceMockTest {
    @MockBean
    private PaymentRepo paymentRepo;

    @Autowired
    PaymentService paymentService;

    @BeforeEach
    void setup(){
        paymentRepo.save(new Payment(2, LocalDate.now(),"savings",20.2,22));
    }

    @AfterEach
    void tearDown(){
        paymentRepo.deleteAll();
    }

    @Test
    public void testFindByIDThrowsException()
    {

        Exception exception = assertThrows(OutOfRangeException.class, () -> {
            Optional<Payment> payment = paymentService.find(0);
        });
    }

    @Test
    public void testFindByIDReturnsPayment() throws OutOfRangeException {
        Payment payment=new Payment(5, LocalDate.now(),"savings",20.2,22);
        when(paymentRepo.count()).thenReturn((long)4);
        when(paymentRepo.findById(1)).thenReturn(Optional.of(payment));
        assertNotEquals(Optional.empty(), paymentService.find(1));


    }

    public void testFindAllPaymentSize() throws OutOfRangeException {
        List<Payment> payments=new ArrayList<>() ;
        Payment payment=new Payment(5, LocalDate.now(),"savings",20.2,22);
        payments.add(payment);
        when(paymentRepo.findAll()).thenReturn(payments);
        assertEquals(1, paymentService.all().size());

    }
    public void testSavePayments() throws OutOfRangeException {
        List<Payment> payments=new ArrayList<>() ;
        Payment payment=new Payment(5, LocalDate.now(),"savings",20.2,22);
        payments.add(payment);
        when(paymentRepo.findAll()).thenReturn(payments);
        assertEquals(1, paymentService.all().size());

    }


}
