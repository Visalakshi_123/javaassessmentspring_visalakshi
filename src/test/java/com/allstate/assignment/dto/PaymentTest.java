package com.allstate.assignment.dto;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PaymentTest {

    @Test
    public void testPaymentConstructorPopulatesData() {

        Payment payment = new Payment(7, LocalDate.now(), "Savings", 12.3, 23);

        assertEquals(7, payment.getId());

        assertEquals(23, payment.getCustid());

        assertEquals("Savings", payment.getType());
        assertEquals(LocalDate.now(), payment.getPaymentdate());

        assertEquals(12.3, payment.getAmount());
    }

    @Test
    public void testPaymentSetData() {

        Payment payment = new Payment(7, LocalDate.now(), "Savings", 12.3, 23);
        payment.setId(23);
        assertEquals(23, payment.getId());
        payment.setCustid(23);
        assertEquals(23, payment.getCustid());
        payment.setType("Savings");
        assertEquals("Savings", payment.getType());
        assertEquals(LocalDate.now(), payment.getPaymentdate());
        payment.setAmount(12.3);
        assertEquals(12.3, payment.getAmount());
        assertEquals("Payment{" +
                "id=" + 23 + ","
                + " paymentdate=" + LocalDate.now()
                + ", type='" + "Savings" + '\''
                + ", amount=" + 12.3
                + ", custid=" + 23 +
                "}",payment.toString());
    }






}
