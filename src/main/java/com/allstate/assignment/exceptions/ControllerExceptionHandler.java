package com.allstate.assignment.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(OutOfRangeException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public  ResponseEntity<String> handleOutofRange(OutOfRangeException ex, WebRequest request) {
        String errorDetails= ex.getMessage() + "URL" + request.getDescription(false);
        return new ResponseEntity(errorDetails, HttpStatus.NOT_FOUND);
    }

//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    public ResponseEntity<String> handleStatus(WebRequest request) {
//
//        String errorDetails=  "Bad request" + request.getDescription(false) + request.getHeaderNames();
//
//        return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
//
//    }

//
//


//    @ResponseStatus(HttpStatus.NOT_FOUND)
//    public class RecordNotFoundException extends RuntimeException
//    {
//        private static final long serialVersionUID = 1L;
//
//        public RecordNotFoundException(String message) {
//            super(message);
//        }
//    }



}
