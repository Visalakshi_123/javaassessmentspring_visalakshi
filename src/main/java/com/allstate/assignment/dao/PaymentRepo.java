package com.allstate.assignment.dao;

import com.allstate.assignment.dto.Payment;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PaymentRepo extends MongoRepository<Payment, Integer> {

}
