package com.allstate.assignment.services;

import com.allstate.assignment.dao.PaymentRepo;
import com.allstate.assignment.dto.Payment;
import com.allstate.assignment.exceptions.OutOfRangeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    PaymentRepo paymentRepo;

    @Override
    public List<Payment> all() {
        return paymentRepo.findAll();
    }

    @Override
    public Optional<Payment> find(int id) throws OutOfRangeException {

        if (id < 1) {
            throw new OutOfRangeException("ID must be over 0 ", 1);
        }

        if (paymentRepo.count() > 0) {
            return paymentRepo.findById(id);
        } else {
            //return paymentRepo.findById(id);
            return null;
        }
    }


    @Override
    public void save(Payment payment) throws OutOfRangeException {
        if (payment.getId() < 1) {
            throw new OutOfRangeException("id must be over 0", 1);
        }
payment.setPaymentdate(LocalDate.now());
        paymentRepo.save(payment);

    }


}
