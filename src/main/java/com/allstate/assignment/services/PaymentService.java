package com.allstate.assignment.services;

import com.allstate.assignment.dto.Payment;
import com.allstate.assignment.exceptions.OutOfRangeException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


public interface PaymentService {

   List<Payment> all();

   Optional<Payment> find(int id) throws OutOfRangeException;
   void save(Payment payment) throws OutOfRangeException;

}
