package com.allstate.assignment.controllers;

import com.allstate.assignment.dto.Payment;
import com.allstate.assignment.exceptions.OutOfRangeException;
import com.allstate.assignment.services.KafkaPublisher;
import com.allstate.assignment.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("api/payment")
public class PaymentController {
    @Autowired
    KafkaPublisher kafkaPublisher;
    @Autowired
    PaymentService paymentService;

    @GetMapping("status")
    public String getStatus() {

        String message= "Payment Service is running";
        return message;

    }

    @GetMapping("find/{id}")
    public Optional<Payment> find(@PathVariable int id) throws OutOfRangeException{
        kafkaPublisher.sendMessage("f1","A search for"+id+"was issued");
        return paymentService.find(id);
    }

    @GetMapping("findAll")
    public List<Payment> findAll() throws OutOfRangeException{
        return paymentService.all();
    }


    @PostMapping("/save")
        public void save(@RequestBody Payment payment) throws OutOfRangeException {
       // Payment payment=new Payment(86,new Date(),"savings",20.2,22);
        paymentService.save(payment);
    }


}
